package helper

import (
	"database/sql"
	"fmt"
)

type ConnectDBConfig struct {
	Host     string
	Port     int
	Database string
	Username string
	Password string
}

func ConnectDB(driver string, config ConnectDBConfig) (*sql.DB, error) {
	dsn := fmt.Sprintf(
		"%s://%s:%s@%s:%d/%s",
		driver,
		config.Username,
		config.Password,
		config.Host,
		config.Port,
		config.Database,
	)
	return sql.Open(driver, dsn)
}

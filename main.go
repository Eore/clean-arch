package main

import (
	"sicepat/app/technologies/database"
	"sicepat/app/technologies/http/restApi"
)

func main() {
	dbc := database.ConnectToPostgresDB(database.PostgresConfig{})
	restApi.StartRestAPI(dbc, 9999)
}

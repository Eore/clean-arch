package hr

import (
	"sicepat/app/entities"
	"sicepat/app/interfaces/repositories/employee"
)

type AddNewEmployeeRepo struct {
	employee.InsertEmployee
}

type NewEmployee struct {
	Name    string
	Address string
}

func AddNewEmployee(repo AddNewEmployeeRepo) func(newEmployee NewEmployee) (string, error) {
	return func(newEmployee NewEmployee) (string, error) {
		employee := entities.NewEmployee(entities.NewEmployeeData{})
		if err := repo.InsertEmployee(employee); err != nil {
			return "", err
		}
		return "NIK", nil
	}
}

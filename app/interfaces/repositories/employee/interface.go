package employee

import "sicepat/app/entities"

type Employee interface {
	InsertEmployee(entities.Employee) error
}

type InsertEmployee func(entities.Employee) error

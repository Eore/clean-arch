package controllers

import (
	"sicepat/app/flows/hr"
)

type RegisterNewEmployeeData struct {
	Token   string
	Name    string
	Address string
}

func RegisterNewEmployee(repo hr.AddNewEmployeeRepo) func(data RegisterNewEmployeeData) error {
	addNewEmployee := hr.AddNewEmployee(repo)
	return func(data RegisterNewEmployeeData) error {
		addNewEmployee(hr.NewEmployee{
			Name:    data.Name,
			Address: data.Address,
		})
		return nil
	}
}

package database

import (
	"database/sql"
	"log"
	"sicepat/helper"
)

type PostgresConfig struct {
	helper.ConnectDBConfig
}

func ConnectToPostgresDB(config PostgresConfig) *sql.DB {
	dbc, err := helper.ConnectDB("postgres", config.ConnectDBConfig)
	if err != nil {
		log.Fatal(err)
	}
	return dbc
}

package router

import (
	"database/sql"
	"sicepat/app/technologies/http/restApi/handler"

	"github.com/gofiber/fiber/v2"
)

func EmployeeRouter(dbc *sql.DB, app *fiber.App) {
	app.Get("/", handler.RegisterNewEmployee(dbc))
}

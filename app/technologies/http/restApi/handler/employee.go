package handler

import (
	"database/sql"
	"sicepat/app/flows/hr"
	"sicepat/app/interfaces/controllers"
	"sicepat/app/interfaces/repositories/employee"

	"github.com/gofiber/fiber/v2"
)

func RegisterNewEmployee(dbc *sql.DB) fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		tx, err := dbc.Begin()
		if err != nil {
			return err
		}
		repo := employee.Postgres{Tx: tx}
		registerNewEmployee := controllers.RegisterNewEmployee(hr.AddNewEmployeeRepo{
			InsertEmployee: repo.InsertEmployee,
		})
		if err := registerNewEmployee(controllers.RegisterNewEmployeeData{}); err != nil {
			tx.Rollback()
			return err
		}
		return tx.Commit()
	}
}

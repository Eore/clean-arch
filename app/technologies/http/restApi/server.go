package restApi

import (
	"database/sql"
	"fmt"
	"sicepat/app/technologies/http/restApi/router"

	"github.com/gofiber/fiber/v2"
)

func StartRestAPI(dbc *sql.DB, port int) {
	app := fiber.New()
	{
		router.EmployeeRouter(dbc, app)
	}
	fmt.Println("RestAPI start on port", port)
	app.Listen(fmt.Sprintf(":%d", port))
}
